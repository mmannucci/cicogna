<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Benvenuta Arianna!</title>
        <link rel='stylesheet' href="${resource(dir:'bootstrap/css', file:'bootstrap.min.css')}" />
        <link rel='stylesheet' href="${resource(dir:'bootstrap/css', file:'bootstrap-responsive.min.css')}" />
        
        <style>
            body {
                background-color: #f9b5de;
            }

            .messaggio {
                color: rgb(90, 86, 90);
            }
            
        </style>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row-fluid">&emsp;</div>
            <div class="row-fluid text-center">
                <div class="span6 offset3">
                    <img class="img-polaroid" src="${resource(dir:'images', file:'cicogna.png')}" />
                </div>
            </div>
            <div class="row-fluid">
                <div class="span6 offset3">


                     <g:form controller="messaggio" action="save">
                      <fieldset>
                        <legend >Lascia un messaggio</legend>

                        <g:if test="${flash.message}">
                        <div class="alert alert-info" role="status">${flash.message}</div>
                        </g:if>
                        <g:if test="${error}">
                        <div class="alert alert-error" role="status">${error}</div>
                        </g:if>

                        <g:hasErrors bean="${messaggioInstance}">
                        <ul class="alert alert-error" role="alert">
                        <g:eachError bean="${messaggioInstance}" var="error">
                        <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                        </g:eachError>
                        </ul>
                        </g:hasErrors>
                        <label>Nome</label>
                        <input type="text" name="nome" value="${messaggioInstance?.nome}"></br>
                        <textarea rows="3" class="input-xxlarge"  name="corpo"  placeholder="Scrivi qui il tuo messaggio">${messaggioInstance?.corpo}</textarea>
                        <label>Codice verifica</label>
                        <input type="text" name="codiceVerifica"><br/>
                        <button type="submit" class="btn" onclick="confirm('Sei sicuro?')">Invia</button>
                      </fieldset>
                    </g:form>
                </div>
            </div>
            
            <div class="row-fluid">
                <div class="span6 offset3">
                    <g:each in="${cicogna.Messaggio.listOrderByDateCreated()}" var="messaggio">
                    <blockquote class="messaggio">
                    <p>${messaggio.corpo}</p>
                    <small>${messaggio.nome} - ${messaggio.dateCreated.format("dd/MM/yyyy - HH:mm")}</small>
                    </blockquote>
                    </g:each>
                </div>
           </div>

            <div class="row-fluid text-center">
                <div class="span8 offset2">
                    <p>
                    <small>Proudly made by Solving Team ;)</small>
                    </p>
                </div>
            </div>
        </div>
        
        
    </body>
</html>
