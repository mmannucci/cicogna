<%@ page import="cicogna.Messaggio" %>



<div class="fieldcontain ${hasErrors(bean: messaggioInstance, field: 'nome', 'error')} required">
	<label for="nome">
		<g:message code="messaggio.nome.label" default="Nome" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nome" required="" value="${messaggioInstance?.nome}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: messaggioInstance, field: 'corpo', 'error')} required">
	<label for="corpo">
		<g:message code="messaggio.corpo.label" default="Corpo" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="corpo" required="" value="${messaggioInstance?.corpo}"/>
</div>

