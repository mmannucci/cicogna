
<%@ page import="cicogna.Messaggio" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'messaggio.label', default: 'Messaggio')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-messaggio" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-messaggio" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="nome" title="${message(code: 'messaggio.nome.label', default: 'Nome')}" />
					
						<g:sortableColumn property="corpo" title="${message(code: 'messaggio.corpo.label', default: 'Corpo')}" />
					
						<g:sortableColumn property="dateCreated" title="${message(code: 'messaggio.dateCreated.label', default: 'Date Created')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${messaggioInstanceList}" status="i" var="messaggioInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${messaggioInstance.id}">${fieldValue(bean: messaggioInstance, field: "nome")}</g:link></td>
					
						<td>${fieldValue(bean: messaggioInstance, field: "corpo")}</td>
					
						<td><g:formatDate date="${messaggioInstance.dateCreated}" /></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${messaggioInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
