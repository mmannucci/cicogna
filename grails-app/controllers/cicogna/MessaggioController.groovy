package cicogna

import org.springframework.dao.DataIntegrityViolationException

class MessaggioController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def save() {
        def messaggioInstance = new Messaggio(params)

        if (params.codiceVerifica != "solving-saluta-arianna") {
              
            render(view: "/cicogna", model: [messaggioInstance: messaggioInstance, error: "Codice verifica errato"])
            return
        }
        if (!messaggioInstance.save(flush: true)) {
            render(view: "/cicogna", model: [messaggioInstance: messaggioInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'messaggio.label', default: 'Messaggio'), messaggioInstance.id])
        redirect(uri:'/')
    }
}
