package cicogna

class Messaggio {

    String nome
    String corpo
    Date dateCreated

    static constraints = {
        nome blank:false
        corpo blank:false, maxSize:1000
    }
}
