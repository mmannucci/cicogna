package cicogna



import org.junit.*
import grails.test.mixin.*

@TestFor(MessaggioController)
@Mock(Messaggio)
class MessaggioControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/messaggio/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.messaggioInstanceList.size() == 0
        assert model.messaggioInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.messaggioInstance != null
    }

    void testSave() {
        controller.save()

        assert model.messaggioInstance != null
        assert view == '/messaggio/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/messaggio/show/1'
        assert controller.flash.message != null
        assert Messaggio.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/messaggio/list'

        populateValidParams(params)
        def messaggio = new Messaggio(params)

        assert messaggio.save() != null

        params.id = messaggio.id

        def model = controller.show()

        assert model.messaggioInstance == messaggio
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/messaggio/list'

        populateValidParams(params)
        def messaggio = new Messaggio(params)

        assert messaggio.save() != null

        params.id = messaggio.id

        def model = controller.edit()

        assert model.messaggioInstance == messaggio
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/messaggio/list'

        response.reset()

        populateValidParams(params)
        def messaggio = new Messaggio(params)

        assert messaggio.save() != null

        // test invalid parameters in update
        params.id = messaggio.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/messaggio/edit"
        assert model.messaggioInstance != null

        messaggio.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/messaggio/show/$messaggio.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        messaggio.clearErrors()

        populateValidParams(params)
        params.id = messaggio.id
        params.version = -1
        controller.update()

        assert view == "/messaggio/edit"
        assert model.messaggioInstance != null
        assert model.messaggioInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/messaggio/list'

        response.reset()

        populateValidParams(params)
        def messaggio = new Messaggio(params)

        assert messaggio.save() != null
        assert Messaggio.count() == 1

        params.id = messaggio.id

        controller.delete()

        assert Messaggio.count() == 0
        assert Messaggio.get(messaggio.id) == null
        assert response.redirectedUrl == '/messaggio/list'
    }
}
